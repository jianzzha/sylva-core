{{- $delete_hook := .Values.delete_hook | default dict }}
{{- if $delete_hook.enabled | default false }}
apiVersion: batch/v1
kind: Job
metadata:
  name: {{ .Release.Name }}-delete-kustomizations
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-delete-policy": before-hook-creation
spec:
  activeDeadlineSeconds: {{ $delete_hook.job_timeout | default 1800 }}
  template:
    spec:
      containers:
      - name: delete-hook
        # renovate: datasource=docker
        image: registry.gitlab.com/sylva-projects/sylva-elements/container-images/kube-job:v1.1.0
        command:
          - "/bin/bash"
          - "-c"
          - |
            set -e

            NS="{{ .Release.Namespace }}"
            NAME="{{ .Release.Name }}"
            WORKLOAD_CLUSTER_KUBECONFIG_SECRET_NAME="{{ .Values.cluster.name }}-kubeconfig"

            function _kubectl {
                kubectl -n $NS $@
            }

            function log_event {
                reason=$1
                message=$2
                if [[ -n $3 ]]; then
                  related_resource_kind=${3%/*}
                  related_resource_name=${3#*/}
                  related_resource_console="/ $3 "
                else
                  related_resource_kind=HelmRelease
                  related_resource_name=$NAME
                  related_resource_console=""
                fi

                echo "=== ($reason) $related_resource_console/ $message"

                kubectl apply -f - <<EOF
            apiVersion: v1
            kind: Event
            metadata:
              name: slyva-units.delete.$NAME.$(printf "%0000x%0000x" $RANDOM $RANDOM)
              namespace: $NS
            type: Normal
            reportingComponent: sylva-units-delete-hook
            reportingInstance: "$NAME"
            message: "$message"
            reason: "$reason"
            firstTimestamp: $(date -Iseconds -u | sed 's/+00:00/Z/')
            involvedObject:
              #apiVersion: $TEMPLATE_RESOURCE_API_VERSION
              kind: $related_resource_kind
              name: $related_resource_name
              namespace: $NS
            #action: delete
            source:
              component: sylva-units-delete-hook
              host: $HOSTNAME
            EOF

            }

            log_event Start "Start of pre-delete-hook"

            echo "=== Disabling the pruning of kustomizations installed on workload cluster ==="
            for KUST in $(
                  _kubectl get kustomization -l app.kubernetes.io/instance=$NAME -o json |
                  jq --arg kubeconfig_secret $WORKLOAD_CLUSTER_KUBECONFIG_SECRET_NAME -r '
                      .items[] | 
                      select(.spec.kubeConfig.secretRef.name == $kubeconfig_secret) |
                      .metadata.name
                    '
                ); do
                log_event PrepareForDeletion "Disable pruning" Kustomization/$KUST
                _kubectl patch kustomization $KUST --field-manager delete-hook --type=merge --patch='{"spec":{"prune":false}}' >/dev/null
            done

            echo "=== Disabling the pruning of helmreleases installed on workload cluster ==="
            for RELEASE in $(
                  _kubectl get  helmreleases -l app.kubernetes.io/instance=$NAME -o json |
                  jq --arg kubeconfig_secret $WORKLOAD_CLUSTER_KUBECONFIG_SECRET_NAME -r '
                    .items[] |
                    select(.spec.kubeConfig.secretRef.name == $kubeconfig_secret) |
                    .metadata.name
                    '
                ); do
                log_event PrepareForDeletion "Suspending helmrelease" HelmRelease/$RELEASE
                _kubectl patch helmrelease $RELEASE --field-manager delete-hook --type=merge --patch='{"spec":{"suspend":true}}' >/dev/null
            done

            ALL_KS=$(_kubectl get kustomizations -l app.kubernetes.io/instance=$NAME -o json)
            CLEANED_KS='[]'

            while true; do
                # Compute remaining kustomizations
                REMAINING_KS=$(jq -r --argjson cleaned "$CLEANED_KS" <<< $ALL_KS '[.items[] | select(.metadata.name as $in | $cleaned | index($in) | not)]')

                if jq -e <<< $REMAINING_KS '. | length == 0' >/dev/null; then
                    log_event Done "All kustomizations have been deleted"
                    exit 0
                fi

                echo "=== Remaining kustomizations ==="
                jq -r <<< $REMAINING_KS '[.[].metadata.name] | join(" ")'

                # Compute the list of kustomizations that don't have dependents
                KS_LIST=$(jq -r --arg ns $NS <<< $REMAINING_KS '[.[] | .metadata.name] -
                     [.[].spec.dependsOn | select(. | length > 0) | .[] | select(.namespace == null or .namespace == "$ns") | .name ]
                     | unique | join(" ")')

                for KUST in $KS_LIST; do
                    log_event Deletion "Has to delete" Kustomization/$KUST
                    _kubectl delete kustomization $KUST 2>/dev/null || true

                    KUST_JSON=$(jq --arg kust $KUST <<< $ALL_KS '.items[] | select(.metadata.name == $kust)')

                    # If a Kustomization does not exist anymore, don't wait the deletion of its resources in the following cases:
                    #  * the Kustomization was targeting a remote cluster
                    #  * no resource would be deleted (empty kustomization)
                    #  * the Kustomization was suspended (in that case Flux controller will not delete anything)
                    #  * the Kustomization had "prune: false" (in that case Flux controller will not delete anything)
                    if ! _kubectl get kustomization $KUST 2>/dev/null &&
                       jq --arg kubeconfig_secret $WORKLOAD_CLUSTER_KUBECONFIG_SECRET_NAME -e <<< $KUST_JSON '
                          (.spec.kubeConfig.secretRef.name == $kubeconfig_secret) or
                          (.status.inventory.entries // [] | length == 0) or
                          (.spec.prune == false) or
                          (.spec.suspend // false)
                        ' >/dev/null; then

                        log_event Deletion "Deletion done" Kustomization/$KUST
                        CLEANED_KS=$(jq --arg kust $KUST <<< $CLEANED_KS '. + [$kust]')

                    # Wait for kustomization's resources to be pruned from API, as kustomize controller is not (yet?) doing it
                    else
                        REMAINING_RES=0

                        while read RES; do
                            RES_JSON=$(kubectl get $RES -o json 2>/dev/null || true)
                            if [[ -n $RES_JSON ]] && ! jq -e <<< $RES_JSON '.metadata.annotations."kustomize.toolkit.fluxcd.io/prune" == "disabled"' >/dev/null; then
                                jq -r <<< $RES_JSON '"Remaining resource: \(.kind) \(if .metadata.namespace != null then "\(.metadata.namespace)/" else "" end)\(.metadata.name)"'
                                REMAINING_RES=1
                            fi
                        # following jq query builds the args for kubectl delete command using kustomization inventory
                        # these args look like "-n capi-system Issuer.cert-manager.io capi-selfsigned-issuer" (namespace and api group are optionnal)
                        done < <(jq -r <<< $KUST_JSON '.status.inventory.entries[].id | split ("_") as $entry |
                            "\(if $entry[0] != "" then "-n \($entry[0])" else "" end) \(if $entry[2] != "" then "\($entry[3]).\($entry[2])" else "\($entry[3])" end) \($entry[1])"')

                        # Mark kustomization as deleted if all resources have been deleted and if the Kustomization does not exist anymore
                        if [[ $REMAINING_RES -eq 0 ]] && ! _kubectl get kustomization $KUST 2>/dev/null; then
                            log_event Deletion "Done" Kustomization/$KUST
                            CLEANED_KS=$(jq --arg kust $KUST <<< $CLEANED_KS '. + [$kust]')
                        fi
                    fi
                done
                sleep 1
            done
        securityContext:
          allowPrivilegeEscalation: false
          capabilities:
            drop:
              - ALL
          runAsNonRoot: true
          runAsUser: 1000
          privileged: false
          seccompProfile:
            type: RuntimeDefault
      restartPolicy: Never
      serviceAccountName: {{ .Release.Name }}-pre-delete-hook-sa
---
apiVersion: v1
kind: ServiceAccount
metadata:
  name: {{ .Release.Name }}-pre-delete-hook-sa
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-weight": "-5"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
---
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: {{ .Release.Name }}-pre-delete-hook-r
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-weight": "-5"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
rules:
- apiGroups: ["kustomize.toolkit.fluxcd.io"]
  resources: ["kustomizations"]
  verbs: ["get", "delete", "list", "watch", "patch"]
- apiGroups: ["helm.toolkit.fluxcd.io"]
  resources: ["helmreleases"]
  verbs: ["get", "delete", "list", "watch", "patch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: {{ .Release.Name }}-pre-delete-hook-rb
  namespace: {{ .Release.Namespace }}
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-weight": "-5"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
subjects:
- kind: ServiceAccount
  name: {{ .Release.Name }}-pre-delete-hook-sa
  namespace: {{ .Release.Namespace }}
roleRef:
  kind: Role
  name: {{ .Release.Name }}-pre-delete-hook-r
  apiGroup: rbac.authorization.k8s.io
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRole
metadata:
  name: {{ .Release.Name }}-{{ .Release.Namespace }}-pre-delete-hook-cr
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-weight": "-5"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["get", "list", "watch"]
- apiGroups: [""]
  resources: ["events"]
  verbs: ["get", "list", "create", "patch"]
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: {{ .Release.Name }}-{{ .Release.Namespace }}-pre-delete-hook-crb
  annotations:
    "helm.sh/hook": pre-delete
    "helm.sh/hook-weight": "-5"
    "helm.sh/hook-delete-policy": before-hook-creation,hook-succeeded
subjects:
- kind: ServiceAccount
  name: {{ .Release.Name }}-pre-delete-hook-sa
  namespace: {{ .Release.Namespace }}
roleRef:
  kind: ClusterRole
  name: {{ .Release.Name }}-{{ .Release.Namespace }}-pre-delete-hook-cr
  apiGroup: rbac.authorization.k8s.io
{{- end }}
