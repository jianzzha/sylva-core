# this file contains values overrides for the rancher-monitoring Helm chart, for prometheus
# it's content comes from https://fluxcd.io/flux/monitoring/custom-metrics/#adding-custom-metrics
# and create custom metrics related to clusters on k8s and rancher level
kube-state-metrics:
  rbac:
    extraRules:
      - apiGroups:
          - source.toolkit.fluxcd.io
          - kustomize.toolkit.fluxcd.io
          - helm.toolkit.fluxcd.io
          - notification.toolkit.fluxcd.io
          - cluster.x-k8s.io
          - provisioning.cattle.io
          - management.cattle.io
        resources:
          - gitrepositories
          - helmrepositories
          - helmcharts
          - ocirepositories
          - kustomizations
          - helmreleases
          - alerts
          - providers
          - receivers
          - clusters
          - projects
        verbs: [ "list", "watch" ]
  customResourceState:
    enabled: true
    config:
      spec:
        resources:
          - groupVersionKind:
              group: management.cattle.io
              version: v3
              kind: Project
            metricNamePrefix: rancher_project
            metrics:
              - name: "info"
                help: "List of current projects in Rancher"
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      project_name: [ spec, displayName ]
                labelsFromPath:
                  project_id: [ metadata, name ]
                  rancher_cluster_id: [ spec, clusterName ]
                  description: [ spec, description ]
          - groupVersionKind:
              group: cluster.x-k8s.io
              version: v1beta1
              kind: Cluster
            metricNamePrefix: capi_cluster
            metrics:
              - name: "info"
                help: "The current state of CAPI cluster resources in K8s."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      capi_cluster_name: [ metadata, name ]
                labelsFromPath:
                  capi_cluster_namespace: [ metadata, namespace ]
                  status: [ status, phase ]
          - groupVersionKind:
              group: provisioning.cattle.io
              version: v1
              kind: Cluster
            metricNamePrefix: rancher_cluster
            metrics:
              - name: "info"
                help: "The current state of rancher provisioned clusters."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      rancher_cluster_name: [ metadata, name ]
                labelsFromPath:
                  cluster_namespace: [ metadata, namespace ]
                  rancher_cluster_id: [ status, clusterName ]
                  ready: [ status, ready ]
          - groupVersionKind:
              group: kustomize.toolkit.fluxcd.io
              version: v1
              kind: Kustomization
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, lastAppliedRevision ]
                  source_name: [ spec, sourceRef, name ]
          - groupVersionKind:
              group: helm.toolkit.fluxcd.io
              version: v2
              kind: HelmRelease
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, lastAppliedRevision ]
                  chart_name: [ spec, chart, spec, chart ]
                  chart_source_name: [ spec, chart, spec, sourceRef, name ]
          - groupVersionKind:
              group: source.toolkit.fluxcd.io
              version: v1
              kind: GitRepository
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, artifact, revision ]
                  url: [ spec, url ]
          - groupVersionKind:
              group: source.toolkit.fluxcd.io
              version: v1
              kind: HelmRepository
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, artifact, revision ]
                  url: [ spec, url ]
          - groupVersionKind:
              group: source.toolkit.fluxcd.io
              version: v1
              kind: HelmChart
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, artifact, revision ]
                  chart_name: [ spec, chart ]
                  chart_version: [ spec, version ]
          - groupVersionKind:
              group: source.toolkit.fluxcd.io
              version: v1beta2
              kind: OCIRepository
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  revision: [ status, artifact, revision ]
                  url: [ spec, url ]
          - groupVersionKind:
              group: notification.toolkit.fluxcd.io
              version: v1beta3
              kind: Alert
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  suspended: [ spec, suspend ]
          - groupVersionKind:
              group: notification.toolkit.fluxcd.io
              version: v1beta3
              kind: Provider
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  suspended: [ spec, suspend ]
          - groupVersionKind:
              group: notification.toolkit.fluxcd.io
              version: v1
              kind: Receiver
            metricNamePrefix: gotk
            metrics:
              - name: "resource_info"
                help: "The current state of a GitOps Toolkit resource."
                each:
                  type: Info
                  info:
                    labelsFromPath:
                      name: [ metadata, name ]
                labelsFromPath:
                  customresource_kind: [ kind ]
                  exported_namespace: [ metadata, namespace ]
                  ready: [ status, conditions, "[type=Ready]", status ]
                  suspended: [ spec, suspend ]
                  webhook_path: [ status, webhookPath ]
