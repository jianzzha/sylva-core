apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: pdb-unhealthy-pod-eviction-check
  annotations:
    kustomize.toolkit.fluxcd.io/force: Enabled
    policies.kyverno.io/title: Check PodDisruptionBudget unhealthyPodEvictionPolicy
    policies.kyverno.io/category: Other
    kyverno.io/kyverno-version: 1.9.0
    kyverno.io/kubernetes-version: "1.24"
    policies.kyverno.io/subject: PodDisruptionBudget
    policies.kyverno.io/description: >-
      The default PodDistruptionBudget configuration forbids evicting unhealthy pods,
      preventing nodes draining. Filling in the unhealthyPodEvictionPolicy field with the value
      AlwaysAllow avoids this.
      A label can be added on the pdb to skip this rule :
        sylvaproject.org/unhealthy-pod-eviction-policy: any
    sylva.org/kyverno-exclude-kube-system: add-matchCondition
spec:
  validationFailureAction: Audit
  webhookConfiguration:
    matchConditions:
    - name: 'is-create-or-update'
      expression: 'request.operation in ["CREATE", "UPDATE"]'
    - name: 'exclude-namespaces' # Exclude calico-system namespaces where PDBs are modified too frequently
      expression: '!(request.namespace in ["calico-system","longhorn-system","kubevirt"])'
  rules:

    - name: pdb-unhealthypodeviction
      match:
        any:
        - resources:
            kinds:
            - PodDisruptionBudget
      preconditions:
        all:
        - key: "{{request.operation || 'BACKGROUND'}}"
          operator: AnyIn
          value:
          - CREATE
          - UPDATE
          - BACKGROUND
        - key: "{{ request.object.metadata.labels.\"sylvaproject.org/unhealthy-pod-eviction-policy\" || '' }}"
          operator: NotEquals
          value: any
        - key: "{{ request.object.metadata.namespace }}"
          operator: AllNotIn
          value:
          # An operator that does not support unhealthyPodEvictionPolicy field configuration is deployed in these namespaces.
          # The kyverno mutation rule is not working in these cases as the operators overwrite continously the mutation.
          # Namespace to be removed once the operators manage unhealthyPodEvictionPolicy field
          - calico-system
          - longhorn-system
          - kubevirt
      validate:
        message: >-
          The matching PodDisruptionBudget for this resource has its unhealthyPodEvictionPolicy value different from
          'AlwaysAllow' which is not permitted.
        deny:
          conditions:
            any:
            - key: "{{ request.object.spec.unhealthyPodEvictionPolicy || 'IfHealthyBudget' }}"
              operator: NotEquals
              value: AlwaysAllow
