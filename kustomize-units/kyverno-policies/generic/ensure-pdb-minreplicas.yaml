apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: pdb-minavailable-check
  annotations:
    kustomize.toolkit.fluxcd.io/force: Enabled
    policies.kyverno.io/title: Check PodDisruptionBudget minAvailable
    policies.kyverno.io/category: Other
    kyverno.io/kyverno-version: 1.9.0
    kyverno.io/kubernetes-version: "1.24"
    policies.kyverno.io/subject: PodDisruptionBudget, Deployment, StatefulSet
    policies.kyverno.io/description: >-
      When a Pod controller which can run multiple replicas is subject to an active PodDisruptionBudget,
      if the replicas field has a value lower or equal to the minAvailable value of the PodDisruptionBudget
      it may prevent voluntary disruptions including Node drains which may impact routine maintenance
      tasks and disrupt operations. This policy checks incoming Deployments and StatefulSets which have
      a matching PodDisruptionBudget to ensure these two values do not match.
    sylva.org/kyverno-exclude-kube-system: add-matchCondition
spec:
  validationFailureAction: Enforce
  background: true
  webhookConfiguration:
    matchConditions:
    - name: 'is-create-or-update'
      expression: 'request.operation in ["CREATE", "UPDATE"]'
    - name: 'exclude-namespaces' # Exclude some namespaces where resources are modified too frequently
      expression: '!(request.namespace in ["calico-system", "minio-monitoring", "minio-logging"])'
  rules:
    - name: pdb-minavailable
      match:
        any:
          - resources:
              kinds:
                - Deployment
                - StatefulSet
      context:
        - name: minavailable
          apiCall:
            urlPath: "/apis/policy/v1/namespaces/{{request.namespace}}/poddisruptionbudgets"
            jmesPath: "items[?label_match(spec.selector.matchLabels, `{{request.object.spec.template.metadata.labels}}`)] | max_by(@, &spec.minAvailable) | spec.minAvailable || `-1`"
      validate:
        message: >-
          The matching PodDisruptionBudget for this resource has its minAvailable value
          greater or equal to the replica count which is not permitted.
        deny:
          conditions:
            any:
              - key: "{{ request.object.spec.replicas }}"
                operator: LessThanOrEquals
                value: "{{ minavailable }}"
---
apiVersion: kyverno.io/v1
kind: ClusterPolicy
metadata:
  name: pdb-minavailable-audit
  annotations:
    kustomize.toolkit.fluxcd.io/force: Enabled
    policies.kyverno.io/title: Check PodDisruptionBudget minAvailable
    policies.kyverno.io/category: Other
    kyverno.io/kyverno-version: 1.9.0
    kyverno.io/kubernetes-version: "1.24"
    policies.kyverno.io/subject: PodDisruptionBudget, Deployment, StatefulSet
    policies.kyverno.io/description: >-
      When a Pod controller which can run multiple replicas is subject to an active PodDisruptionBudget,
      if the replicas field has a value lower or equal to the minAvailable value of the PodDisruptionBudget
      it may prevent voluntary disruptions including Node drains which may impact routine maintenance
      tasks and disrupt operations. This policy checks incoming Deployments and StatefulSets which have
      a matching PodDisruptionBudget to ensure these two values do not match.
spec:
  validationFailureAction: Audit
  admission: false
  background: true
  rules:
    - name: pdb-minavailable
      match:
        any:
          - resources:
              kinds:
                - Deployment
                - StatefulSet
      context:
        - name: minavailable
          apiCall:
            urlPath: "/apis/policy/v1/namespaces/{{request.namespace}}/poddisruptionbudgets"
            jmesPath: "items[?label_match(spec.selector.matchLabels, `{{request.object.spec.template.metadata.labels}}`)] | max_by(@, &spec.minAvailable) | spec.minAvailable || `-1`"
      validate:
        message: >-
          The matching PodDisruptionBudget for this resource has its minAvailable value
          greater or equal to the replica count which is not permitted.
        deny:
          conditions:
            any:
              - key: "{{ request.object.spec.replicas }}"
                operator: LessThanOrEquals
                value: "{{ minavailable }}"
